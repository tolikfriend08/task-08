package com.epam.rd.java.basic.task3;

import java.util.List;

public class Company {

    private String companyName;
    private String country;
    private List<City> city;

    public String getCompanyName() {
        return companyName;
    }

    public String getCountry() {
        return country;
    }

    public List<City> getCity() {
        return city;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCity(List<City> city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Company{" +
                "companyName='" + companyName + '\'' +
                ", country='" + country + '\'' +
                ", city=" + city +
                '}';
    }
}
