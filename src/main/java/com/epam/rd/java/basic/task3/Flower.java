package com.epam.rd.java.basic.task3;

import java.util.Arrays;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Flower {

    private String name;
    private Soil soil;
    private String origin;
    private String stemColour;
    private String leafColour;
    private int aveLenFlower;
    private String aveLenFlowerMeasure;
    private int temperature;
    private String temperatureMeasure;
    private LightRequiring lightRequiring;
    private int watering;
    private String wateringMeasure;
    private Multiplying multiplying;


    public Flower() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Soil getSoil() {
        return soil;
    }

    public void setSoil(Soil soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(int aveLenFlower) {
        if (temperature < 0) throw new IllegalArgumentException("Only positive values are allowed! ");
        this.aveLenFlower = aveLenFlower;
    }

    public String getAveLenFlowerMeasure() {
        return aveLenFlowerMeasure;
    }

    public void setAveLenFlowerMeasure(String aveLenFlowerMeasure) {
        this.aveLenFlowerMeasure = aveLenFlowerMeasure;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        if (temperature < 0) throw new IllegalArgumentException("Only positive values are allowed! ");
        this.temperature = temperature;
    }

    public String getTemperatureMeasure() {
        return temperatureMeasure;
    }

    public void setTemperatureMeasure(String temperatureMeasure) {
        this.temperatureMeasure = temperatureMeasure;
    }

    public LightRequiring getLightRequiring() {
        return lightRequiring;
    }

    public void setLightRequiring(LightRequiring lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        if (temperature < 0) throw new IllegalArgumentException("Only positive values are allowed! ");
        this.watering = watering;
    }

    public String getWateringMeasure() {
        return wateringMeasure;
    }

    public void setWateringMeasure(String wateringMeasure) {
        this.wateringMeasure = wateringMeasure;
    }

    public Multiplying getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(Multiplying multiplying) {
        this.multiplying = multiplying;
    }


    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil=" + soil +
                ", origin='" + origin + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                " '" + aveLenFlowerMeasure + '\'' +
                ", temperature=" + temperature +
                " '" + temperatureMeasure + '\'' +
                ", lightRequiring=" + lightRequiring +
                ", watering=" + watering +
                " '" + wateringMeasure + '\'' +
                ", multiplying=" + multiplying +
                '}';
    }

    public enum Soil {
        SOIL_1("подзолистая"),
        SOIL_2("грунтовая"),
        SOIL_3("дерново-подзолистая");

        private final String soil;

        Soil(String soil) {
            this.soil = soil;
        }

        public static Soil getSoilByValue(String textContent) {
            return Arrays.stream(Soil.values()).filter(s -> s.soil.equals(textContent)).findFirst().orElseThrow();
        }

        @Override
        public String toString() {
            return "'" + soil + "'";
        }
    }


    public enum LightRequiring {
        YES("yes"), NO("no");

        private final String lightRequiring;

        LightRequiring(String lightRequiring) {
            this.lightRequiring = lightRequiring;
        }

        public static LightRequiring getLightRequiringByValue(String textContent) {
            return Arrays.stream(LightRequiring.values()).filter(s -> s.lightRequiring.equals(textContent)).findFirst().orElseThrow();
        }

        @Override
        public String toString() {
            return "'" + lightRequiring + "'";
        }
    }


    public enum Multiplying {
        MULTIPLYING_1("листья"),
        MULTIPLYING_3("черенки"),
        MULTIPLYING_2("семена");

        private final String multiplying;

        Multiplying(String multiplying) {
            this.multiplying = multiplying;
        }

        public static Multiplying getMultiplyingByValue(String textContent) {
            return Arrays.stream(Multiplying.values()).filter(s -> s.multiplying.equals(textContent)).findFirst().orElseThrow();
        }

        @Override
        public String toString() {
            return "'" + multiplying + "'";
        }
    }
}


