package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task3.City;
import com.epam.rd.java.basic.task3.Company;
import com.epam.rd.java.basic.task3.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private static final String FLOWER = "flower";
    private static final String NAME = "name";
    private static final String SOIL = "soil";
    private static final String ORIGIN = "origin";
    private static final String VISUAL_PARAMETERS = "visualParameters";
    private static final String STEM_COLOUR = "stemColour";
    private static final String LEAF_COLOUR = "leafColour";
    private static final String AVERAGE_LENGTH_OF_FLOWER = "aveLenFlower";
    private static final String MEASURE = "measure";
    private static final String GROWING_TIPS = "growingTips";
    private static final String TEMPERATURE = "tempreture";
    private static final String LIGHTING = "lighting";
    private static final String LIGHT_REQUIRING = "lightRequiring";
    private static final String WATERING = "watering";
    private static final String MULTIPLYING = "multiplying";

    private String xmlFileName;
    private static final String XSD_SCHEME_FILE = "input.xsd";

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    /**
     * Return the DOM-tree Document object created from XML file
     *
     * @return A new DOM Document object
     */
    public Document parseXMLDOM() {
        // Создается построитель документа
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        dbf.setValidating(false);


        DocumentBuilder db = null;
        Document document = null;
        try {
            // Создается дерево DOM документа из файла
            db = dbf.newDocumentBuilder();
            document = db.parse(this.xmlFileName);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }

        //проверка на валидность xml документа
        try {
            validXmlViaDOM(document);
        } catch (IOException | SAXException e) {
            System.out.println("XML file is not valid! " + e.getMessage());
        }

        return document;
    }

    /**
     * Return new List of Flower objects parsed from XMl file
     *
     * @param document to read from
     * @return A new List of Flower objects
     */
    public List<Flower> getFlowersFromXML(Document document) {

        List<Flower> list = new ArrayList<>();

        //get nodelist of flowers from document object
        NodeList flowersNodeList = document.getElementsByTagName(FLOWER);
        for (int i = 0; i < flowersNodeList.getLength(); i++) {
            Flower flower = new Flower();
            Node nFlower = flowersNodeList.item(i);

            if (nFlower.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) nFlower;

                flower.setName(element.getElementsByTagName(NAME).item(0).getTextContent());
                flower.setSoil(Flower.Soil.getSoilByValue(element.getElementsByTagName(SOIL).item(0).getTextContent()));
                flower.setOrigin(element.getElementsByTagName(ORIGIN).item(0).getTextContent());

                flower.setStemColour(element.getElementsByTagName(STEM_COLOUR).item(0).getTextContent());
                flower.setLeafColour(element.getElementsByTagName(LEAF_COLOUR).item(0).getTextContent());
                flower.setAveLenFlower(Integer.parseInt(element.getElementsByTagName(AVERAGE_LENGTH_OF_FLOWER).item(0).getTextContent()));
                flower.setAveLenFlowerMeasure(element.getElementsByTagName(AVERAGE_LENGTH_OF_FLOWER).item(0).getAttributes().getNamedItem(MEASURE).getTextContent());

                flower.setTemperature(Integer.parseInt(element.getElementsByTagName(TEMPERATURE).item(0).getTextContent()));
                flower.setTemperatureMeasure(element.getElementsByTagName(TEMPERATURE).item(0).getAttributes().getNamedItem(MEASURE).getTextContent());
                flower.setLightRequiring(Flower.LightRequiring
                        .getLightRequiringByValue(element.getElementsByTagName(LIGHTING).item(0)
                                .getAttributes().getNamedItem(LIGHT_REQUIRING).getTextContent()));
                flower.setWatering(Integer.parseInt(element.getElementsByTagName(WATERING).item(0).getTextContent()));
                flower.setWateringMeasure(element.getElementsByTagName(WATERING).item(0).getAttributes().getNamedItem(MEASURE).getTextContent());

                flower.setMultiplying(Flower.Multiplying.getMultiplyingByValue(element.getElementsByTagName(MULTIPLYING).item(0).getTextContent()));
            }

            list.add(flower);
        }

        return list;
    }

    /**
     * Return the sorted list of Flowers using the flower name like argument for sorting
     *
     * @param flowers list to be sorted
     */
    public void compareByName(List<Flower> flowers) {
        flowers.sort(Comparator.comparing(Flower::getName));
    }

    /**
     * Method to create xml file using DOM
     *
     * @param document to read from
     * @param outputXML xml file to which info to be written
     */
    public void createXMLDOM(Document document, String outputXML) {
        try (FileOutputStream fos = new FileOutputStream(outputXML)) {
            Transformer tr = TransformerFactory.newInstance().newTransformer();
            DOMSource source = new DOMSource(document);

            StreamResult result = new StreamResult(fos);
            tr.transform(source, result);
        } catch (TransformerException | IOException e) {
            e.printStackTrace(System.out);
        }
    }


    public void validXmlViaDOM(Document document) throws IOException, SAXException {

        String schemaLang = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory factory = SchemaFactory.newInstance(schemaLang);
        Schema schema = factory.newSchema(new File(XSD_SCHEME_FILE));
        Validator validator = schema.newValidator();
        validator.validate(new DOMSource(document));

    }

}
