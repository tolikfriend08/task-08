package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task3.City;
import com.epam.rd.java.basic.task3.Company;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.*;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stax.StAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXControllerCompany extends DefaultHandler {

    private static final String XSD_SCHEME_FILE = "myInput.xsd";

    private static final String COMPANY_NAME = "companyName";
    private static final String COUNTRY = "country";
    private static final String CITY = "city";
    private static final String CITY_NAME = "cityName";
    private static final String POPULATION = "population";
    private static final String PHONE_CODE = "phoneCode";

    private String xmlFileName;

    public STAXControllerCompany(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // https://www.baeldung.com/java-stax

    /**
     * Return new Company object parsed from XMl file using StAX parser
     *
     * @return A new Company object
     */
    public Company parseXMLStAX() {

        //validate xml
        try {
            validXmlViaStAX(xmlFileName);
        } catch (IOException | SAXException | XMLStreamException e) {
            System.out.println("XML file is not valid! " + e.getMessage());
        }

        Company company = new Company();
        List<City> cityList = new ArrayList<>();
        City city = new City();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        XMLEventReader eventReader = null;
        try {
            eventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
            while (eventReader.hasNext()) {

                XMLEvent nextEvent = eventReader.nextEvent();

                if (nextEvent.isStartElement()) {
                    StartElement startElement = nextEvent.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case COMPANY_NAME:
                            nextEvent = eventReader.nextEvent();
                            company.setCompanyName(nextEvent.asCharacters().getData());
                            break;
                        case COUNTRY:
                            nextEvent = eventReader.nextEvent();
                            company.setCountry(nextEvent.asCharacters().getData());
                            break;
                        case CITY:
                            nextEvent = eventReader.nextEvent();
                            city = new City();
                            break;
                        case CITY_NAME:
                            nextEvent = eventReader.nextEvent();
                            city.setCityName(nextEvent.asCharacters().getData());
                            break;
                        case POPULATION:
                            nextEvent = eventReader.nextEvent();
                            city.setPopulation(Integer.parseInt(nextEvent.asCharacters().getData()));
                            break;
                        case PHONE_CODE:
                            nextEvent = eventReader.nextEvent();
                            city.setPhoneCode(nextEvent.asCharacters().getData());
                            break;
                    }

                }
                if (nextEvent.isEndElement()) {
                    EndElement endElement = nextEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals(CITY)) {
                        cityList.add(city);
                    }
                }

            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }

        company.setCity(cityList);

        return company;
    }


    /**
     * Return the sorted list of cities from Company using the city name as first parameter
     * and the city population as a second parameter
     */
    public void compareCityByNameAndPopulation(Company company) {
        company.getCity().sort(Comparator.comparing(City::getCityName).thenComparing(City::getPopulation));
    }


    /**
     * Method to create xml file using StAX
     *
     * @param outputXML xml file to which info to be written
     */
    public void createXMLStAX(Company company, String outputXML) {

        XMLOutputFactory output = XMLOutputFactory.newInstance();
        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEventWriter eventWriter = null;

        try {
            // StAX Iterator API
            eventWriter = output.createXMLEventWriter(new FileOutputStream(outputXML));

            eventWriter.add(eventFactory.createStartDocument("UTF-8", "1.0"));

            eventWriter.add(eventFactory.createStartElement("ts", "http://myNameSpace", "company"));
            eventWriter.add(eventFactory.createNamespace("ts", "http://myNameSpace"));
            eventWriter.add(eventFactory.createNamespace("xsi", "http://www.w3.org/2001/XMLSchema"));

            eventWriter.add(eventFactory.createStartElement("","",COMPANY_NAME));
            eventWriter.add(eventFactory.createCharacters(company.getCompanyName()));
            eventWriter.add(eventFactory.createEndElement("","",COMPANY_NAME));

            eventWriter.add(eventFactory.createStartElement("","",COUNTRY));
            eventWriter.add(eventFactory.createCharacters(company.getCountry()));
            eventWriter.add(eventFactory.createEndElement("","",COUNTRY));

            for (City city : company.getCity()){
                eventWriter.add(eventFactory.createStartElement("","",CITY));

                eventWriter.add(eventFactory.createStartElement("","",CITY_NAME));
                eventWriter.add(eventFactory.createCharacters(city.getCityName()));
                eventWriter.add(eventFactory.createEndElement("","",CITY_NAME));

                eventWriter.add(eventFactory.createStartElement("","",POPULATION));
                eventWriter.add(eventFactory.createCharacters(String.valueOf(city.getPopulation())));
                eventWriter.add(eventFactory.createEndElement("","",POPULATION));

                eventWriter.add(eventFactory.createStartElement("","",PHONE_CODE));
                eventWriter.add(eventFactory.createCharacters(city.getPhoneCode()));
                eventWriter.add(eventFactory.createEndElement("","",PHONE_CODE));

                eventWriter.add(eventFactory.createEndElement("","",CITY));
            }
            // end here.
            eventWriter.add(eventFactory.createEndElement("ts", "http://myNameSpace", "company"));

            eventWriter.add(eventFactory.createEndDocument());

            eventWriter.flush();
            eventWriter.close();
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            //transform xml from one row to normal view
            formatXML(outputXML);
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            e.printStackTrace();
        }

    }



    private static void formatXML(String file) throws ParserConfigurationException, TransformerException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new InputStreamReader(new FileInputStream(file))));

        Transformer xformer = TransformerFactory.newInstance().newTransformer();
// Sets XML formatting
        xformer.setOutputProperty(OutputKeys.METHOD, "xml");
// Sets indent
        xformer.setOutputProperty(OutputKeys.INDENT, "yes");
        Source source = new DOMSource(document);
        Result result = new StreamResult(new File(file));
        xformer.transform(source, result);
    }



    public void validXmlViaStAX(String xmlFileName) throws IOException, XMLStreamException, SAXException {
        String schemaLang = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory factory = SchemaFactory.newInstance(schemaLang);
        Schema schema = factory.newSchema(new File(XSD_SCHEME_FILE));
        Validator validator = schema.newValidator();

        Source xmlSource = new StAXSource(XMLInputFactory.newFactory().createXMLEventReader(new FileInputStream(xmlFileName)));
        validator.validate(xmlSource, null);
    }

}