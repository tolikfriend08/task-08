package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task3.City;
import com.epam.rd.java.basic.task3.Company;
import com.epam.rd.java.basic.task8.handler.SAXParserHandlerCompany;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;


/**
 * Controller for SAX parser.
 */
public class SAXControllerCompany extends DefaultHandler {

    private String xmlFileName;
    private static final String XSD_SCHEME_FILE = "myInput.xsd";

    private static final String COMPANY_NAME = "companyName";
    private static final String COUNTRY = "country";
    private static final String CITY = "city";
    private static final String CITY_NAME = "cityName";
    private static final String POPULATION = "population";
    private static final String PHONE_CODE = "phoneCode";


    public SAXControllerCompany(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE


    /**
     * Return new Company object parsed from XMl file using SAX parser
     *
     * @return A new Company object
     */
    public Company parseXMLSAX() {
        Company company = new Company();

        SAXParserFactory factory = SAXParserFactory.newInstance();

        //проверка валидирования в ручном режиме
/////////////////////////////////////////////
        factory.setValidating(false);
        factory.setNamespaceAware(true);

        try{
            validXmlViaSAX(xmlFileName);
        } catch (IOException | SAXException e) {
            System.out.println("XML file is not valid! " + e.getMessage());
        }

//////////////////////////////////////////////


        SAXParser parser = null;

        SAXParserHandlerCompany handler = new SAXParserHandlerCompany();
        try {
            parser = factory.newSAXParser();
            parser.parse(xmlFileName, handler);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }


        company = handler.getCompany();

        return company;
    }

    /**
     * Return the sorted list of cities from Company using the city population like argument for descending sorting
     */
    public void compareCityByPopulation(Company company) {
        company.getCity().sort(Comparator.comparing(City::getPopulation).reversed());
    }


    /**
     * Method to create xml file using SAX
     *
     * @param outputXML xml file to which info to be written
     */
    public void createXMLSAX(Company company, String outputXML) {

        String newLine = System.getProperty("line.separator");

        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xsw = null;
        try {
            xsw = xof.createXMLStreamWriter(new FileWriter(outputXML));
            xsw.writeStartDocument("utf-8", "1.0");
//            xsw.writeCharacters(newLine);

            xsw.writeStartElement("ts", "company", "http://myNameSpace");
            xsw.writeNamespace("ts", "http://myNameSpace");
            xsw.writeNamespace("xsi", "http://www.w3.org/2001/XMLSchema");
//            xsw.writeAttribute("http://www.w3.org/2001/XMLSchema","schemaLocation","http://myNameSpace myInput.xsd");

            xsw.writeStartElement(COMPANY_NAME);
            xsw.writeCharacters(company.getCompanyName());
            xsw.writeEndElement();
            xsw.writeStartElement(COUNTRY);
            xsw.writeCharacters(company.getCountry());
            xsw.writeEndElement();

            for (City city : company.getCity()) {
                xsw.writeStartElement(CITY);
                xsw.writeStartElement(CITY_NAME);
                xsw.writeCharacters(city.getCityName());
                xsw.writeEndElement();
                xsw.writeStartElement(POPULATION);
                xsw.writeCharacters(Integer.toString(city.getPopulation()));
                xsw.writeEndElement();
                xsw.writeStartElement(PHONE_CODE);
                xsw.writeCharacters(city.getPhoneCode());
                xsw.writeEndElement();
                xsw.writeEndElement();
            }

            xsw.writeEndElement();
            xsw.writeEndDocument();
            xsw.flush();
        } catch (Exception e) {
            System.err.println("Unable to write the file: " + e.getMessage());
        } finally {
            try {
                if (xsw != null) {
                    xsw.close();
                }
            } catch (Exception e) {
                System.err.println("Unable to close the file: " + e.getMessage());
            }
        }

        try {
            //transform xml from one row to normal view
            formatXML(outputXML);
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            e.printStackTrace();
        }

    }


    private static void formatXML(String file) throws ParserConfigurationException, TransformerException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new InputStreamReader(new FileInputStream(file))));

        Transformer xformer = TransformerFactory.newInstance().newTransformer();
// Sets XML formatting
        xformer.setOutputProperty(OutputKeys.METHOD, "xml");
// Sets indent
        xformer.setOutputProperty(OutputKeys.INDENT, "yes");
        Source source = new DOMSource(document);
        Result result = new StreamResult(new File(file));
        xformer.transform(source, result);
    }


    public void validXmlViaSAX(String xmlFileName) throws SAXException, IOException {

        Path xmlPath = Paths.get(xmlFileName);
        Reader reader = null;
        String schemaLang = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory factory = SchemaFactory.newInstance(schemaLang);
        try {
            reader = Files.newBufferedReader(xmlPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Schema schema = null;
        Validator validator = null;
        SAXSource source = null;

        try {
            schema = factory.newSchema(new File(XSD_SCHEME_FILE));
            validator = schema.newValidator();
            source = new SAXSource(new InputSource(reader));
        } catch (SAXException e) {
            e.printStackTrace();
        }

        validator.validate(source);
    }


}

