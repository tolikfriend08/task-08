package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task3.City;
import com.epam.rd.java.basic.task3.Company;
import com.epam.rd.java.basic.task3.Flower;
import com.epam.rd.java.basic.task8.handler.SAXParserHandler;
import com.epam.rd.java.basic.task8.handler.SAXParserHandlerCompany;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private static final String FLOWERS = "flowers";
    private static final String FLOWER = "flower";
    private static final String NAME = "name";
    private static final String SOIL = "soil";
    private static final String ORIGIN = "origin";
    private static final String VISUAL_PARAMETERS = "visualParameters";
    private static final String STEM_COLOUR = "stemColour";
    private static final String LEAF_COLOUR = "leafColour";
    private static final String AVERAGE_LENGTH_OF_FLOWER = "aveLenFlower";
    private static final String MEASURE = "measure";
    private static final String GROWING_TIPS = "growingTips";
    private static final String TEMPERATURE = "tempreture";
    private static final String LIGHTING = "lighting";
    private static final String LIGHT_REQUIRING = "lightRequiring";
    private static final String WATERING = "watering";
    private static final String MULTIPLYING = "multiplying";

    private String xmlFileName;
    private static final String XSD_SCHEME_FILE = "input.xsd";


    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE


    /**
     * Return a new List of Flowers parsed from XMl file using SAX parser
     *
     * @return A new List of Flower objects
     */
    public List<Flower> parseXMLSAX() {
       List <Flower> flowerList = new ArrayList<>();

        SAXParserFactory factory = SAXParserFactory.newInstance();

        //проверка валидирования в ручном режиме
/////////////////////////////////////////////
        factory.setValidating(false);
        factory.setNamespaceAware(true);

        try{
            validXmlViaSAX(xmlFileName);
        } catch (IOException | SAXException e) {
            System.out.println("XML file is not valid! " + e.getMessage());
        }

//////////////////////////////////////////////


        SAXParser parser = null;

        SAXParserHandler handler = new SAXParserHandler();
        try {
            parser = factory.newSAXParser();
            parser.parse(xmlFileName, handler);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }


        flowerList = handler.getFlowerList();

        return flowerList;
    }

    /**
     * Return the sorted list of Flowers using the average length of flower like argument for descending sorting
     */
    public void compareByAverageLength(List <Flower> flowers) {
        flowers.sort(Comparator.comparing(Flower::getAveLenFlower).reversed());
    }


    /**
     * Method to create xml file using SAX
     *
     * @param flowers the list of Flower objects to be written to xml file
     * @param outputXML xml file to which info to be written
     */
    public void createXMLSAX(List <Flower> flowers, String outputXML) {

//        String newLine = System.getProperty("line.separator");

        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xsw = null;
        try {
            xsw = xof.createXMLStreamWriter(new FileWriter(outputXML));
            xsw.writeStartDocument("utf-8", "1.0");
//            xsw.writeCharacters(newLine);

            xsw.writeStartElement( "", FLOWERS, "http://www.nure.ua");
            xsw.writeDefaultNamespace( "http://www.nure.ua");
            xsw.writeNamespace( "xsi","http://www.w3.org/2001/XMLSchema-instance");

            for (Flower flower : flowers) {
                xsw.writeStartElement(FLOWER);

                xsw.writeStartElement(NAME);
                xsw.writeCharacters(flower.getName());
                xsw.writeEndElement();

                xsw.writeStartElement(SOIL);
                xsw.writeCharacters(flower.getSoil().toString().replaceAll("'",""));
                xsw.writeEndElement();

                xsw.writeStartElement(ORIGIN);
                xsw.writeCharacters(flower.getOrigin());
                xsw.writeEndElement();

                xsw.writeStartElement(VISUAL_PARAMETERS);

                xsw.writeStartElement(STEM_COLOUR);
                xsw.writeCharacters(flower.getStemColour());
                xsw.writeEndElement();

                xsw.writeStartElement(LEAF_COLOUR);
                xsw.writeCharacters(flower.getLeafColour());
                xsw.writeEndElement();

                xsw.writeStartElement(AVERAGE_LENGTH_OF_FLOWER);
                xsw.writeAttribute(MEASURE, flower.getAveLenFlowerMeasure());
                xsw.writeCharacters(String.valueOf(flower.getAveLenFlower()));
                xsw.writeEndElement();

                xsw.writeEndElement();
                xsw.writeStartElement(GROWING_TIPS);

                xsw.writeStartElement(TEMPERATURE);
                xsw.writeAttribute(MEASURE, flower.getTemperatureMeasure());
                xsw.writeCharacters(String.valueOf(flower.getTemperature()));
                xsw.writeEndElement();

                xsw.writeStartElement(LIGHTING);
                xsw.writeAttribute(LIGHT_REQUIRING, flower.getLightRequiring().toString().replaceAll("'",""));
                xsw.writeEndElement();

                xsw.writeStartElement(WATERING);
                xsw.writeAttribute(MEASURE, flower.getWateringMeasure());
                xsw.writeCharacters(String.valueOf(flower.getWatering()));
                xsw.writeEndElement();

                xsw.writeEndElement();

                xsw.writeStartElement(MULTIPLYING);
                xsw.writeCharacters(flower.getMultiplying().toString().replaceAll("'",""));
                xsw.writeEndElement();

                xsw.writeEndElement();
            }

            xsw.writeEndElement();
            xsw.writeEndDocument();
            xsw.flush();
        } catch (Exception e) {
            System.err.println("Unable to write the file: " + e.getMessage());
        } finally {
            try {
                if (xsw != null) {
                    xsw.close();
                }
            } catch (Exception e) {
                System.err.println("Unable to close the file: " + e.getMessage());
            }
        }

        try {
            //transform xml from one row to normal view
            formatXML(outputXML);
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            e.printStackTrace();
        }

    }


    private static void formatXML(String file) throws ParserConfigurationException, TransformerException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new InputStreamReader(new FileInputStream(file))));

        Transformer xformer = TransformerFactory.newInstance().newTransformer();
// Sets XML formatting
        xformer.setOutputProperty(OutputKeys.METHOD, "xml");
// Sets indent
        xformer.setOutputProperty(OutputKeys.INDENT, "yes");
        Source source = new DOMSource(document);
        Result result = new StreamResult(new File(file));
        xformer.transform(source, result);
    }


    public void validXmlViaSAX(String xmlFileName) throws SAXException, IOException {

        Path xmlPath = Paths.get(xmlFileName);
        Reader reader = null;
        String schemaLang = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory factory = SchemaFactory.newInstance(schemaLang);
        try {
            reader = Files.newBufferedReader(xmlPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Schema schema = null;
        Validator validator = null;
        SAXSource source = null;

        try {
            schema = factory.newSchema(new File(XSD_SCHEME_FILE));
            validator = schema.newValidator();
            source = new SAXSource(new InputSource(reader));
        } catch (SAXException e) {
            e.printStackTrace();
        }

        validator.validate(source);
    }


}

