package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task3.Company;
import com.epam.rd.java.basic.task3.Flower;
import com.epam.rd.java.basic.task8.controller.*;
import org.w3c.dom.Document;

import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		Document domDocument = domController.parseXMLDOM();
		List<Flower> flowerList1 = domController.getFlowersFromXML(domDocument);
		System.out.println("//////////// DOM /////////////");
		System.out.println(flowerList1);

		// sort (case 1)
		// PLACE YOUR CODE HERE
		domController.compareByName(flowerList1);
		System.out.println("//////////// DOM sorted /////////////");
		System.out.println(flowerList1);

		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.createXMLDOM(domDocument, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		List <Flower> flowerList2 = saxController.parseXMLSAX();
		System.out.println("//////////// SAX /////////////");
		System.out.println(flowerList2);

		// sort  (case 2)
		// PLACE YOUR CODE HERE
		saxController.compareByAverageLength(flowerList2);
		System.out.println("//////////// SAX sorted /////////////");
		System.out.println(flowerList2);

		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		saxController.createXMLSAX(flowerList2, outputXmlFile);





		////////////////////////////////////////////////////////
		// StAX
		//var1 from lection
//		XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
//		XMLStreamReader streamReader = xmlInputFactory.createXMLStreamReader(new FileInputStream("myInput.xml"));
//		while (streamReader.hasNext()){
//			String s = streamReader.getElementText();
//		}

		//var2
//		XMLEventReader xmlReader =
//				xmlInputFactory.createXMLEventReader(new FileInputStream("myInput.xml"));
//		while (xmlReader.hasNext()){
//			XMLEvent event = xmlReader.nextEvent();
//			if(event.isStartElement()){
//				StartElement stElement = event.asStartElement();
//				QName name = stElement.getName();
//				System.out.println(name);
//			}
//		}
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		List <Flower> flowerList3 = staxController.parseXMLStAX();
		System.out.println("//////////// StAX /////////////");
		System.out.println(flowerList3);

		// sort  (case 3)
		// PLACE YOUR CODE HERE
		staxController.compareByWatering(flowerList3);
		System.out.println("//////////// StAX sorted /////////////");
		System.out.println(flowerList3);

		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		staxController.createXMLStAX(flowerList3, outputXmlFile);
	}

}
