package com.epam.rd.java.basic.task8.handler;

import com.epam.rd.java.basic.task3.City;
import com.epam.rd.java.basic.task3.Company;
import com.epam.rd.java.basic.task3.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXParserHandler extends DefaultHandler {

    //https://www.baeldung.com/java-sax-parser

    private static final String FLOWER = "flower";
    private static final String NAME = "name";
    private static final String SOIL = "soil";
    private static final String ORIGIN = "origin";
    private static final String VISUAL_PARAMETERS = "visualParameters";
    private static final String STEM_COLOUR = "stemColour";
    private static final String LEAF_COLOUR = "leafColour";
    private static final String AVERAGE_LENGTH_OF_FLOWER = "aveLenFlower";
    private static final String MEASURE = "measure";
    private static final String GROWING_TIPS = "growingTips";
    private static final String TEMPERATURE = "tempreture";
    private static final String LIGHTING = "lighting";
    private static final String LIGHT_REQUIRING = "lightRequiring";
    private static final String WATERING = "watering";
    private static final String MULTIPLYING = "multiplying";

    private List<Flower> flowerList = new ArrayList<>();

    private StringBuilder elementValue;


    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (elementValue == null) {
            elementValue = new StringBuilder();
        } else {
            elementValue.append(ch, start, length);
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (FLOWER.equals(qName)) {
            flowerList.add(new Flower());
        } else if (AVERAGE_LENGTH_OF_FLOWER.equals(qName)) {
            //get attribute value
            latestFlower().setAveLenFlowerMeasure(attributes.getValue(MEASURE));
            elementValue = new StringBuilder();
        } else if (TEMPERATURE.equals(qName)) {
            latestFlower().setTemperatureMeasure(attributes.getValue(MEASURE));
            elementValue = new StringBuilder();
        } else if (LIGHTING.equals(qName)) {
            latestFlower().setLightRequiring(Flower.LightRequiring.getLightRequiringByValue(attributes.getValue(LIGHT_REQUIRING)));
        } else if (WATERING.equals(qName)) {
            latestFlower().setWateringMeasure(attributes.getValue(MEASURE));
            elementValue = new StringBuilder();
        } else {
            elementValue = new StringBuilder();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case NAME:
                latestFlower().setName(elementValue.toString());
                break;
            case SOIL:
                latestFlower().setSoil(Flower.Soil.getSoilByValue(elementValue.toString()));
                break;
            case ORIGIN:
                latestFlower().setOrigin(elementValue.toString());
                break;
            case STEM_COLOUR:
                latestFlower().setStemColour(elementValue.toString());
                break;
            case LEAF_COLOUR:
                latestFlower().setLeafColour(elementValue.toString());
                break;
            case AVERAGE_LENGTH_OF_FLOWER:
                latestFlower().setAveLenFlower(Integer.parseInt(elementValue.toString()));
                break;
            case TEMPERATURE:
                latestFlower().setTemperature(Integer.parseInt(elementValue.toString()));
                break;
            case WATERING:
                latestFlower().setWatering(Integer.parseInt(elementValue.toString()));
                break;
            case MULTIPLYING:
                latestFlower().setMultiplying(Flower.Multiplying.getMultiplyingByValue(elementValue.toString()));
                break;
        }
    }


    private Flower latestFlower() {
        int latestFlowerIndex = flowerList.size() - 1;
        return flowerList.get(latestFlowerIndex);
    }


    public List<Flower> getFlowerList() {
        return flowerList;
    }
}
