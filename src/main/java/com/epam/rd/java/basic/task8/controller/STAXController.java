package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task3.City;
import com.epam.rd.java.basic.task3.Company;
import com.epam.rd.java.basic.task3.Flower;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stax.StAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private static final String FLOWERS = "flowers";
    private static final String FLOWER = "flower";
    private static final String NAME = "name";
    private static final String SOIL = "soil";
    private static final String ORIGIN = "origin";
    private static final String VISUAL_PARAMETERS = "visualParameters";
    private static final String STEM_COLOUR = "stemColour";
    private static final String LEAF_COLOUR = "leafColour";
    private static final String AVERAGE_LENGTH_OF_FLOWER = "aveLenFlower";
    private static final String MEASURE = "measure";
    private static final String GROWING_TIPS = "growingTips";
    private static final String TEMPERATURE = "tempreture";
    private static final String LIGHTING = "lighting";
    private static final String LIGHT_REQUIRING = "lightRequiring";
    private static final String WATERING = "watering";
    private static final String MULTIPLYING = "multiplying";


    private static final String XSD_SCHEME_FILE = "input.xsd";

    private String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // https://www.baeldung.com/java-stax

    /**
     * Return a new List of Flowers parsed from XMl file using StAX parser
     *
     * @return A new List of Flower objects
     */
    public List<Flower> parseXMLStAX() {

        //validate xml
        try {
            validXmlViaStAX(xmlFileName);
        } catch (IOException | SAXException | XMLStreamException e) {
            System.out.println("XML file is not valid! " + e.getMessage());
        }

        Flower flower = new Flower();
        List<Flower> flowerList = new ArrayList<>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        XMLEventReader eventReader = null;
        try {
            eventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
            while (eventReader.hasNext()) {

                XMLEvent nextEvent = eventReader.nextEvent();

                if (nextEvent.isStartElement()) {
                    StartElement startElement = nextEvent.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case FLOWER:
                            nextEvent = eventReader.nextEvent();
                            flower = new Flower();
                            break;
                        case NAME:
                            nextEvent = eventReader.nextEvent();
                            flower.setName(nextEvent.asCharacters().getData());
                            break;
                        case SOIL:
                            nextEvent = eventReader.nextEvent();
                            flower.setSoil(Flower.Soil.getSoilByValue(nextEvent.asCharacters().getData()));
                            break;
                        case ORIGIN:
                            nextEvent = eventReader.nextEvent();
                            flower.setOrigin(nextEvent.asCharacters().getData());
                            break;
                        case STEM_COLOUR:
                            nextEvent = eventReader.nextEvent();
                            flower.setStemColour(nextEvent.asCharacters().getData());
                            break;
                        case LEAF_COLOUR:
                            nextEvent = eventReader.nextEvent();
                            flower.setLeafColour(nextEvent.asCharacters().getData());
                            break;
                        case AVERAGE_LENGTH_OF_FLOWER:
                            nextEvent = eventReader.nextEvent();
                            Attribute attribute = startElement.getAttributeByName(new QName(MEASURE));
                            flower.setAveLenFlowerMeasure(attribute.getValue());
                            flower.setAveLenFlower(Integer.parseInt(nextEvent.asCharacters().getData()));
                            break;
                        case TEMPERATURE:
                            nextEvent = eventReader.nextEvent();
                            flower.setTemperatureMeasure(startElement.getAttributeByName(new QName(MEASURE)).getValue());
                            flower.setTemperature(Integer.parseInt(nextEvent.asCharacters().getData()));
                            break;
                        case LIGHTING:
                            nextEvent = eventReader.nextEvent();
                            flower.setLightRequiring(Flower.LightRequiring
                                    .getLightRequiringByValue(startElement.getAttributeByName(new QName(LIGHT_REQUIRING)).getValue()));
                            break;
                        case WATERING:
                            nextEvent = eventReader.nextEvent();
                            flower.setWateringMeasure(startElement.getAttributeByName(new QName(MEASURE)).getValue());
                            flower.setWatering(Integer.parseInt(nextEvent.asCharacters().getData()));
                            break;
                        case MULTIPLYING:
                            nextEvent = eventReader.nextEvent();
                            flower.setMultiplying(Flower.Multiplying.getMultiplyingByValue(nextEvent.asCharacters().getData()));
                            break;
                    }

                }
                if (nextEvent.isEndElement()) {
                    EndElement endElement = nextEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals(FLOWER)) {
                        flowerList.add(flower);
                    }
                }

            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }

        return flowerList;
    }


    /**
     * Return the sorted list of Flowers using the flower's watering as a parameter
     */
    public void compareByWatering(List<Flower> flowers) {
        flowers.sort(Comparator.comparing(Flower::getWatering));
    }


    /**
     * Method to create xml file using StAX
     *
     * @param flowers   the list of Flower objects to be written to xml file
     * @param outputXML xml file to which info to be written
     */
    public void createXMLStAX(List<Flower> flowers, String outputXML) {

        XMLOutputFactory output = XMLOutputFactory.newInstance();
        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEventWriter eventWriter = null;

        try {
            // StAX Iterator API
            eventWriter = output.createXMLEventWriter(new FileOutputStream(outputXML));

            eventWriter.add(eventFactory.createStartDocument("UTF-8", "1.0"));

            eventWriter.add(eventFactory.createStartElement("", "http://www.nure.ua", FLOWERS));
            eventWriter.add(eventFactory.createNamespace("", "http://www.nure.ua"));
            eventWriter.add(eventFactory.createNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance"));

            for (Flower flower : flowers) {

                eventWriter.add(eventFactory.createStartElement("", "", FLOWER));

                eventWriter.add(eventFactory.createStartElement("", "", NAME));
                eventWriter.add(eventFactory.createCharacters(flower.getName()));
                eventWriter.add(eventFactory.createEndElement("", "", NAME));

                eventWriter.add(eventFactory.createStartElement("", "", SOIL));
                eventWriter.add(eventFactory.createCharacters(flower.getSoil().toString().replaceAll("'","")));
                eventWriter.add(eventFactory.createEndElement("", "", SOIL));

                eventWriter.add(eventFactory.createStartElement("", "", ORIGIN));
                eventWriter.add(eventFactory.createCharacters(flower.getOrigin()));
                eventWriter.add(eventFactory.createEndElement("", "", ORIGIN));

                eventWriter.add(eventFactory.createStartElement("", "", VISUAL_PARAMETERS));

                eventWriter.add(eventFactory.createStartElement("", "", STEM_COLOUR));
                eventWriter.add(eventFactory.createCharacters(flower.getStemColour()));
                eventWriter.add(eventFactory.createEndElement("", "", STEM_COLOUR));

                eventWriter.add(eventFactory.createStartElement("", "", LEAF_COLOUR));
                eventWriter.add(eventFactory.createCharacters(flower.getLeafColour()));
                eventWriter.add(eventFactory.createEndElement("", "", LEAF_COLOUR));

                eventWriter.add(eventFactory.createStartElement("", "", AVERAGE_LENGTH_OF_FLOWER));
                eventWriter.add(eventFactory.createAttribute(MEASURE, flower.getAveLenFlowerMeasure()));
                eventWriter.add(eventFactory.createCharacters(String.valueOf(flower.getAveLenFlower())));
                eventWriter.add(eventFactory.createEndElement("", "", AVERAGE_LENGTH_OF_FLOWER));

                eventWriter.add(eventFactory.createEndElement("", "", VISUAL_PARAMETERS));

                eventWriter.add(eventFactory.createStartElement("", "", GROWING_TIPS));

                eventWriter.add(eventFactory.createStartElement("", "", TEMPERATURE));
                eventWriter.add(eventFactory.createAttribute(MEASURE, flower.getTemperatureMeasure()));
                eventWriter.add(eventFactory.createCharacters(String.valueOf(flower.getTemperature())));
                eventWriter.add(eventFactory.createEndElement("", "", TEMPERATURE));

                eventWriter.add(eventFactory.createStartElement("", "", LIGHTING));
                eventWriter.add(eventFactory.createAttribute(LIGHT_REQUIRING, flower.getLightRequiring().toString().replaceAll("'","")));
                eventWriter.add(eventFactory.createEndElement("", "", LIGHTING));

                eventWriter.add(eventFactory.createStartElement("", "", WATERING));
                eventWriter.add(eventFactory.createAttribute(MEASURE, flower.getWateringMeasure()));
                eventWriter.add(eventFactory.createCharacters(String.valueOf(flower.getWatering())));
                eventWriter.add(eventFactory.createEndElement("", "", WATERING));

                eventWriter.add(eventFactory.createEndElement("", "", GROWING_TIPS));

                eventWriter.add(eventFactory.createStartElement("", "", MULTIPLYING));
                eventWriter.add(eventFactory.createCharacters(flower.getMultiplying().toString().replaceAll("'","")));
                eventWriter.add(eventFactory.createEndElement("", "", MULTIPLYING));

                eventWriter.add(eventFactory.createEndElement("", "", FLOWER));
            }
            // end here.
            eventWriter.add(eventFactory.createEndElement("", "http://www.nure.ua", FLOWERS));

            eventWriter.add(eventFactory.createEndDocument());

            eventWriter.flush();
            eventWriter.close();
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            //transform xml from one row to normal view
            formatXML(outputXML);
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            e.printStackTrace();
        }

    }


    private static void formatXML(String file) throws ParserConfigurationException, TransformerException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new InputStreamReader(new FileInputStream(file))));

        Transformer xformer = TransformerFactory.newInstance().newTransformer();
// Sets XML formatting
        xformer.setOutputProperty(OutputKeys.METHOD, "xml");
// Sets indent
        xformer.setOutputProperty(OutputKeys.INDENT, "yes");
        Source source = new DOMSource(document);
        Result result = new StreamResult(new File(file));
        xformer.transform(source, result);
    }


    public void validXmlViaStAX(String xmlFileName) throws IOException, XMLStreamException, SAXException {
        String schemaLang = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory factory = SchemaFactory.newInstance(schemaLang);
        Schema schema = factory.newSchema(new File(XSD_SCHEME_FILE));
        Validator validator = schema.newValidator();

        Source xmlSource = new StAXSource(XMLInputFactory.newFactory().createXMLEventReader(new FileInputStream(xmlFileName)));
        validator.validate(xmlSource, null);
    }

}