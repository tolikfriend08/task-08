package com.epam.rd.java.basic.task8.handler;

import com.epam.rd.java.basic.task3.City;
import com.epam.rd.java.basic.task3.Company;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXParserHandlerCompany extends DefaultHandler {

    //https://www.baeldung.com/java-sax-parser

    private static final String COMPANY_NAME = "companyName";
    private static final String COUNTRY = "country";
    private static final String CITY = "city";
    private static final String CITY_NAME = "cityName";
    private static final String POPULATION = "population";
    private static final String PHONE_CODE = "phoneCode";

    private Company company = new Company();
    private City city = new City();
    private static List<City> cityList = new ArrayList<>();

    private StringBuilder elementValue;


    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (elementValue == null) {
            elementValue = new StringBuilder();
        } else {
            elementValue.append(ch, start, length);
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (CITY.equals(qName)) {
            cityList.add(new City());
        } else {
            elementValue = new StringBuilder();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case COMPANY_NAME:
                company.setCompanyName(elementValue.toString());
                break;
            case COUNTRY:
                company.setCountry(elementValue.toString());
                break;
            case CITY_NAME:
                latestCity().setCityName(elementValue.toString());
                break;
            case POPULATION:
                latestCity().setPopulation(Integer.parseInt(elementValue.toString()));
                break;
            case PHONE_CODE:
                latestCity().setPhoneCode(elementValue.toString());
                break;
        }
    }


    private static City latestCity() {
        int latestCityIndex = cityList.size() - 1;
        return cityList.get(latestCityIndex);
    }


    public Company getCompany(){
        company.setCity(cityList);
        return company;
    }
}
