package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task3.City;
import com.epam.rd.java.basic.task3.Company;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMControllerCompany {

    private String xmlFileName;
    private static final String XSD_SCHEME_FILE = "myInput.xsd";

    public DOMControllerCompany(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    /**
     * Return the DOM-tree Document object created from XML file
     *
     * @return A new DOM Document object
     */
    public Document parseXMLDOM() {
        // Создается построитель документа
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        dbf.setValidating(false);


        DocumentBuilder db = null;
        Document document = null;
        try {
            // Создается дерево DOM документа из файла
            db = dbf.newDocumentBuilder();
            document = db.parse(this.xmlFileName);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }

        //проверка на валидность xml документа
        try {
            validXmlViaDOM(document);
        } catch (IOException | SAXException e) {
            System.out.println("XML file is not valid! " + e.getMessage());
        }

        return document;
    }

    /**
     * Return new Company object parsed from XMl file
     *
     * @param document to read from
     * @return A new Company object
     */
    public Company getCompanyFromXML(Document document) {
        Company company = new Company();
        List<City> list = new ArrayList<>();

        //получаем имя компании
        company.setCompanyName(document.getElementsByTagName("companyName").item(0).getTextContent());
        //и страну
        company.setCountry(document.getElementsByTagName("country").item(0).getTextContent());

        NodeList cityList = document.getElementsByTagName("city");
        for (int i = 0; i < cityList.getLength(); i++) {
            City city = new City();
            Node nCity = cityList.item(i);
            // Если нода не текст, то это city - заходим внутрь
            if (nCity.getNodeType() != Node.TEXT_NODE) {
                NodeList cityProps = nCity.getChildNodes();
                for (int j = 0; j < cityProps.getLength(); j++) {
                    Node nCityProp = cityProps.item(j);
                    switch (nCityProp.getNodeName()) {
                        case ("cityName"):
                            city.setCityName(nCityProp.getTextContent());
                            break;
                        case ("population"):
                            city.setPopulation(Integer.parseInt(nCityProp.getTextContent()));
                            break;
                        case ("phoneCode"):
                            city.setPhoneCode(nCityProp.getTextContent());
                            break;
                    }
                }
            }
            list.add(city);
        }

        company.setCity(list);

        return company;
    }

    /**
     * Return the sorted list of cities from Company using the city name like argument for sorting
     *
     * @param company which list of cities to be sorted
     */
    public void compareCityByName(Company company) {
        company.getCity().sort(Comparator.comparing(City::getCityName));
    }

    /**
     * Method to create xml file using DOM
     *
     * @param document  to read from
     * @param outputXML xml file to which info to be written
     */
    public void createXMLDOM(Document document, String outputXML) {
        try (FileOutputStream fos = new FileOutputStream(outputXML)){
            Transformer tr = TransformerFactory.newInstance().newTransformer();
            DOMSource source = new DOMSource(document);

            StreamResult result = new StreamResult(fos);
            tr.transform(source, result);
        } catch (TransformerException | IOException e) {
            e.printStackTrace(System.out);
        }
    }


    public void validXmlViaDOM(Document document) throws IOException, SAXException {

        String schemaLang = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory factory = SchemaFactory.newInstance(schemaLang);
        Schema schema = factory.newSchema(new File(XSD_SCHEME_FILE));
        Validator validator = schema.newValidator();
        validator.validate(new DOMSource(document));

    }

}
